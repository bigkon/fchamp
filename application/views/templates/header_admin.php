<html>
<head>
    <title><?php echo $title ?> - F-Champ</title>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

    <style>
        .divider-vertical {
            height: 50px;
            margin: 0 9px;
            border-left: 1px solid #F2F2F2;
            border-right: 1px solid #FFF;
        }

        #formLogin > *{
            margin-bottom: 5px;
        }
    </style>

    <?php if( isset($active_page) ) { ?>
        <script>
            $(function(){
                $('#<?php echo $active_page; ?>').addClass('active');
            });
        </script>
    <?php } ?>
</head>
<body>

<div class="container">
    <nav class="navbar navbar-default navbar-collapse" role="navigation">
        <a href="#" class="navbar-brand">F-Champ</a>
        <ul class="nav navbar-nav">
            <li class="divider-vertical"></li>
            <li id="tourneys_link"><a href="<?php echo site_url('tourneys/show'); ?>">Турниры</a></li>
            <li id="teams_link"><a href="<?php echo site_url('teams/show_all'); ?>">Команды</a></li>
            <li id="users_link"><a href="<?php echo site_url('account/show_all'); ?>">Пользователи</a></li>
            <li class="divider-vertical"></li>
            <li id="admin_link" class="dropdown"><a data-toggle="dropdown" href="#">Администратор<i class="caret"></i></a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="<?php echo site_url('admin/tourney/show/1'); ?>">Турниры</a></li>
                    <li><a href="#">Команды</a></li>
                </ul>
            </li>
        </ul>

        <ul class="nav navbar-nav navbar-right">
            <li><div>Вы вошли, как <i><a class="badge" href="<?php echo site_url('account/show'); ?>">
                        <?php echo $this->session->userdata('fname') . ' ' . $this->session->userdata('lname'); ?></a>
                    </i></div>
                <a href="<?php echo site_url('account/logout') ;?>" class="pull-right input-sm">Выйти</a>
            </li>
        </ul>
    </nav>
