<html>
<head>
    <title><?php echo $title ?> - F-Champ</title>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

    <!--VK-->
    <script type="text/javascript" src="//vk.com/js/api/openapi.js?113"></script>
    <script type="text/javascript">
        VK.init({apiId: 4376528});

        function vk_login_callback(response) {
            if (response.session) {
                var uid = response.session.user.id;
                $.get($('#login_vk').attr('href'), 'uid=' + uid + '&auth_net=' + 1, net_auth_resp, 'json');
            } else {
                $('#h_errors_display').append('<p>Пользователь не авторизирован</p>').fadeIn().fadeOut(3000);
            }
        }

        function net_auth_resp(resp) {
            if( resp.result ) {
                window.location.reload();
            } else {
                $('#h_errors_display > p').remove();
                $('#h_errors_display').append('<p>' + resp.error + '</p>').fadeIn().fadeOut(3000);
            }
        }

    <!--end VK-->


    <!--FB-->
        window.fbAsyncInit = function() {
            FB.init({
                appId      : '748863955144578',
                xfbml      : false,
                version    : 'v2.0'
            });
            FB.getLoginStatus();
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/ru_RU/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    <!--end FB-->
    </script>
    <script>

        $(function(){

            $('#submitLogin').click(function(e){
                e.preventDefault();

                $form = $('#formLogin');
                $.post($form.attr('action'), $form.serialize(), function(resp){
                   if( !resp.result ) {
                       $('#h_errors_display > p').remove();
                       $('#h_errors_display').append('<p>' + resp.error + '</p>').fadeIn().fadeOut(2000);
                       $('#login_menu_show').click();
                   } else {
                       window.location.reload();
                   }
                }, 'json');
            });

        <?php if( isset($active_page) ) { ?>
            $('#<?php echo $active_page; ?>').addClass('active');
        <?php } ?>

            var h_err = $('#h_errors_display');
            if(h_err.children().length <= 1)
                h_err.hide();

            $('#login_vk').click(function(e){
                e.preventDefault();
                VK.Auth.login(vk_login_callback);
            });

            $('#login_fb').click(function(e) {
                e.preventDefault();
                FB.login(function(response) {
                    if( response.status == 'connected' ) {
                        FB.api('/me?fields=id,email,first_name,last_name', function(response) {
                            var uid = response.id;
                            $.get( $('#login_fb').attr('href'), 'uid=' + uid + '&auth_net=' + 2, net_auth_resp, 'json');
                        });
                    } else {
                        alert(response.status);
                    }

                }, {scope: 'public_profile'});
            });
        });
    </script>

    <style>
        .divider-vertical {
            height: 50px;
            margin: 0 9px;
            border-left: 1px solid #F2F2F2;
            border-right: 1px solid #FFF;
        }

        #formLogin > *{
            margin-bottom: 5px;
        }
    </style>
</head>
<body>

<div class="container">
    <nav class="navbar navbar-default navbar-collapse" role="navigation">
        <a href="#" class="navbar-brand">FChamp</a>
        <ul class="nav navbar-nav">
            <li class="divider-vertical"></li>
            <li id="tourneys_link"><a href="<?php echo site_url('tourneys/show'); ?>">Турниры</a></li>
            <li id="teams_link"><a href="<?php echo site_url('teams/show_all'); ?>">Команды</a></li>
            <li id="users_link"><a href="<?php echo site_url('account/show_all'); ?>">Пользователи</a></li>
        </ul>




        <ul class="nav navbar-nav navbar-right">
            <li><div id="h_errors_display" class="text-danger">
                    <a type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</a>
                </div></li>
            <li><a href="<?php echo site_url('account/create'); ?>">Регистрация</a></li>
            <li class="dropdown">
                <a class="dropdown-toggle" href="#" data-toggle="dropdown" id="login_menu_show">Войти</a>
                <div class="dropdown-menu" style="padding: 17px;">
                    <form class="form" id="formLogin" method="post" action="<?php echo site_url('account/login')?>">
                        <input name="emailLogin" id="emailLogin" type="text" placeholder="Email">
                        <input name="passwordLogin" id="passwordLogin" type="password" placeholder="Пароль">
                        <button type="submit" id="submitLogin" class="btn btn-default">Войти</button>
                    </form>
                    <div class="divider"></div>
                    <div class="text-center">
                        <a id="login_vk" href="<?php echo site_url('account/login_net'); ?>">
                            <img src="/images/vk.JPG" alt="Войти используя ВК" width="32px"
                                 title="Войти, используя ВК" />
                        </a>
                        <a id="login_fb" href="<?php echo site_url('account/login_net'); ?>">
                            <img src="/images/fb.png" alt="Войти используя Facebook" width="32px"
                                 title="Войти, используя Facebook" />
                        </a>

                        <div class="divider"></div>
                        <a class="btn btn-default" href="http://fchamp.com/index.php/account/create">Регистрация</a>
                    </div>
                </div>
            </li>
        </ul>
    </nav>
