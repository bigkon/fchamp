<html>
<head>
    <title><?php echo $title ?> - F-Champ</title>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="//vk.com/js/api/openapi.js?113"></script>
    <script src="/js/header_default.js"></script>

    <script type="text/javascript">
        <?php if( isset($active_page) ) { ?>
            $('#<?php echo $active_page; ?>').addClass('active');
        <?php } ?>
    </script>

    <style>
        .divider-vertical {
            height: 50px;
            margin: 0 9px;
            border-left: 1px solid #F2F2F2;
            border-right: 1px solid #FFF;
        }

        #formLogin > *{
            margin-bottom: 5px;
        }
    </style>
</head>
<body>

<div class="container">
    <nav class="navbar navbar-default navbar-collapse" role="navigation">
        <a href="#" class="navbar-brand">FChamp</a>
        <ul class="nav navbar-nav">
            <li class="divider-vertical"></li>
            <li id="tourneys_link"><a href="<?php echo site_url('tourneys/show'); ?>">Турниры</a></li>
            <li id="teams_link"><a href="<?php echo site_url('teams/show_all'); ?>">Команды</a></li>
            <li id="users_link"><a href="<?php echo site_url('account/show_all'); ?>">Пользователи</a></li>
        </ul>




        <ul class="nav navbar-nav navbar-right">
            <li><div id="h_errors_display" class="text-danger">
                    <a type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</a>
                </div></li>
            <li><a href="<?php echo site_url('account/create'); ?>">Регистрация</a></li>
            <li class="dropdown">
                <a class="dropdown-toggle" href="#" data-toggle="dropdown" id="login_menu_show">Войти</a>
                <div class="dropdown-menu" style="padding: 17px;">
                    <form class="form" id="formLogin" method="post" action="<?php echo site_url('account/login')?>">
                        <input name="emailLogin" id="emailLogin" type="text" placeholder="Email">
                        <input name="passwordLogin" id="passwordLogin" type="password" placeholder="Пароль">
                        <button type="submit" id="submitLogin" class="btn btn-default">Войти</button>
                    </form>
                    <div class="divider"></div>
                    <div class="text-center">
                        <a id="login_vk" href="<?php echo site_url('account/login_net'); ?>">
                            <img src="/images/vk.JPG" alt="Войти используя ВК" width="32px"
                                 title="Войти, используя ВК" />
                        </a>
                        <a id="login_fb" href="<?php echo site_url('account/login_net'); ?>">
                            <img src="/images/fb.png" alt="Войти используя Facebook" width="32px"
                                 title="Войти, используя Facebook" />
                        </a>

                        <div class="divider"></div>
                        <a class="btn btn-default" href="http://fchamp.com/index.php/account/create">Регистрация</a>
                    </div>
                </div>
            </li>
        </ul>
    </nav>
