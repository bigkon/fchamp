<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script>
    $(function(){
        $('#error_messages').hide();
        $('#votes-list').sortable().disableSelection();
        $('#vote-save').click(function(e){
            e.preventDefault();
            var votes = Array();
            $('#votes-list').children().each(function(){
                votes.push($(this).data('id'));
            });
            $.post(
                $(location).attr('href'),
                {votes: JSON.stringify(votes)},
                function(resp) {
                    var msg_div = $('#error_messages');
                    if( !resp.success ) {
                        msg_div.removeClass('alert-success').addClass('alert-danger').append('<p>' + resp.error_message + '</p>');
                    } else {
                        msg_div.removeClass('alert-danger').addClass('alert-success').append('<p>Результаты приняты</p>');
                    }
                    msg_div.fadeIn();
                },
                'json'
            );
        });
    });
</script>

<style>
    .score {
        border-left: 1px solid gray;
        width: 10%;
        text-align: center;
        font-size: large;
    }

    .vote-item {
        background-color: #eeeeee;
        font-size: 16px;
        margin: 5px;
        padding: 10px;
    }

    #progress {
        background-image: linear-gradient(rgb(0, 255, 0), rgb(255, 255, 0), rgb(255,0,0));
        height: 50%;
        width: 10px;
        box-shadow: 0px 0px 4px 0px rgba(0, 0, 0, 0.57);
    }
</style>
<h1 class="text-center">Голосование</h1>
<div class="col-lg-6 col-lg-offset-3">
    <div id="error_messages" class="alert">
        <a href="#" class="close" data-dismiss="alert" aria-hidden="true">&times;</a>
    </div>
    <div id="votes-list">
        <?php foreach($votes as $vote): ?>
            <div class="vote-item" data-id="<?php echo $vote['id_user_to'] ?>">
                <div class="pull-right score"><?php echo $vote['value']; ?></div>
                <div><a href="<?php echo site_url("account/show/{$vote['id_user_to']}"); ?>"><?php echo $vote['lname']. ' ' . $vote['fname']; ?></a></div>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="pull-right">
        <a href="#" class="btn btn-success" id="vote-save">Сохранить</a>
    </div>
</div>
<div class="col-lg-1">
    <div id="progress"></div>
</div>
