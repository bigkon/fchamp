<style>
    .info-block{
        border-bottom: 1px solid #999;
        padding-bottom: 50px;
        margin-bottom: 20px;
    }
</style>
<h1 class="text-center">Личный кабинет</h1>

<div class="row">
<div class="col-lg-2 col-lg-offset-1">
        <div class="text-center">
            <div class="panel panel-default">
                <img class="pull-right" src="/images/users/<?php echo $photo; ?>" alt="Фотка" width="100%"/>
                <h3><?php echo $fname . ' ' . $lname; ?></h3>
                <?php
                if($self) {
                    echo '<div class="panel-footer">'.
                        '<a href="' . site_url('/account/edit') . '" class="btn btn-default">Редактировать</a>'.
                        '</div>';
                }?>
            </div>
        </div>
        <?php if($self && !empty($available_votes)): ?>
            <div>
                <div class="panel panel-success">
                    <div class="panel-heading">Голосования</div>
                    <div class="panel-body">
                        <ul class="list-unstyled">
                            <?php foreach($available_votes as $v): ?>
                                <li>
                                    <a href="<?php echo site_url("account/vote/{$v['id']}"); ?>">
                                        <?php echo $v['name']; ?>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
        <?php endif; ?>

</div>
<div class="row">
    <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-7">
                <div class="panel panel-default">
                    <div class="panel-heading">Турниры</div>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Турнир</th>
                            <th>Статус</th>
                            <th>Команд</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $status=array('Не начался','Идет','Голосование','Закончился');
                        foreach($tourney_list as $tourney): ?>
                            <tr>
                                <td><a href="<?php echo site_url("tourneys/show/{$tourney['t_id']}"); ?>"><?php echo $tourney['t_name']; ?></a></td>
                                <td><?php echo $status[$tourney['t_state']]; ?></td>
                                <td><?php echo $tourney['teams']; ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="panel panel-default">
                    <div class="panel-heading">Команды</div>
                    <table class="table table-striped">
                        <?php foreach($teams as $team): ?>
                            <tr><td><a href="<?php echo site_url("teams/show/{$team['id']}"); ?>"><?php echo $team['name']; ?></a></td></tr>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

