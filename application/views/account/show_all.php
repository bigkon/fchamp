<style>
    #users_container div {
        margin: 10px;
        min-height: 32px;
    }

    #users_container div:hover {
            background-color: #EBF3EC;
        }


    #users_container>:nth-child(even) {
        background-color: #f9f9f9;
    }

    div>a{
        text-decoration: none;
        color: inherit;
    }

</style>

<div class="col-lg-6 col-lg-offset-3">
    <div class="row thumbnail">
        <div class="col-lg-3"><strong>Фильтры</strong></div>
        <div class="col-lg-9">
            <div class="input-group">
                <input class="form-control" placeholder="Имя" name="lname_search">
                <span class="input-group-btn">
                    <button type="button" class="btn btn-default" onclick="search_btn_click()">Искать</button>
                </span>
            </div>
        </div>
    </div>
    <div id="users_container">
<?php
$acc_info = site_url('account/show') . '/';
foreach($users as $user) {
?>
    <div>
        <img class="pull-right" src="/images/users/<?php echo $user->photo; ?>" alt="Пользователь" width="32px" height="32px" />
        <div><a href="<?php echo $acc_info . $user->id;?>"><?php echo $user->fname . ' ' . $user->lname; ?></a></div>
    </div>
<?php } ?>
    </div>
</div>

<script>
    function search_btn_click() {
        var pattern = $('input[name=lname_search]').val();
        var children = $('#users_container').children();
        for(child in children) {
            if(children[child].innerText.indexOf(pattern) < 0)
                children[child].style.display = 'none';
            else
                children[child].style.display = 'inherit';
        }
    }
</script>