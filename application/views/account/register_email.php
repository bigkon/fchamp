<h1>Регистрация выполнена</h1>
<div class="alert alert-default">На <strong><?php echo html_escape($email); ?></strong> отправлена ссылка для завершения регистрации.</div>
<?php
if(isset($message)) {
    echo "<div>$message</div>";
}
?>