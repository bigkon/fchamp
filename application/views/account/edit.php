<h1 class="text-center">
    Редактирование данных
</h1>

<div class="col-lg-4 col-lg-offset-4">
    <div class="alert alert-danger" id="errors_display">
        <a type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</a>
        <?php echo validation_errors(); ?>
        <?php
        if(isset($error_pswd))
            echo "<p>$error_pswd</p>";
        if(isset($error_photo))
            echo "<p>$error_photo</p>";
        ?>
    </div>
    <?php

    echo form_open_multipart();

    echo '<div class="form-group">';
    echo form_label('Имя', 'fname');
    echo form_input(array('name' => 'fname', 'id' => 'fname', 'placeholder' => 'Имя', 'class' => 'form-control'), $fname);
    echo '</div>';

    echo '<div class="form-group">';
    echo form_label('Фамилия', 'lname');
    echo form_input(array('name' => 'lname', 'id' => 'lname', 'placeholder' => 'Фамилия', 'class' => 'form-control'), $lname);
    echo '</div>';

    echo '<div class="form-group">';
    echo form_label('Фото', 'photo');
    echo form_upload(array('name' => 'photo', 'id' => 'photo', 'placeholder' => 'Фото', 'class' => 'form-control', 'accept' => 'image/*'));
    echo '<div id="user_photo"><img src="/images/users/' . $photo . '" width="150px" height="150px"/>';
    echo '</div>';
    echo '</div>';

    echo '<hr/>';
    echo '<div class="form-group">';
    echo form_label('Старый пароль', 'pswd_old');
    echo form_password(array('name' => 'pswd_old', 'id' => 'pswd_old', 'placeholder' => 'Пароль', 'class' => 'form-control'));
    echo '</div>';

    echo '<div class="form-group">';
    echo form_label('Новый пароль', 'pswd');
    echo form_password(array('name' => 'pswd', 'id' => 'pswd', 'placeholder' => 'Новый пароль', 'class' => 'form-control'));
    echo '<br/>';
    echo form_password(array('name' => 'pswd_check', 'id' => 'pswd_check', 'placeholder' => 'Новый пароль снова', 'class' => 'form-control'));
    echo '</div>';

    echo form_submit(array('name' => 'submit'), 'Изменить');
    echo form_close();

    ?>
</div>

<script>
    $(function() {
        $errors = $('#errors_display');
        if($errors.children().length <= 1)
            $errors.hide();
    });
</script>