<script src="/js/net_reg.js"></script>
<div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4">
    <h2 class="text-center">Регистрация</h2>

<?php
    echo '<div class="alert alert-danger" id="errors_display"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</a>';
        echo validation_errors();
        if(isset($error)){
            echo '<p>'.$error.'</p>';
        }
    echo '</div>';

    echo '<div class="col-lg-9 col-md-9 col-sm-9">';
        echo form_open('', array('role'=>'form', 'style'=>'padding: 5px; padding-right: 25px; border-right: 1px solid #eee;'));

            echo '<div class="form-group">';
            echo form_label('Email', 'email');
            echo form_input($email, set_value('email'));
            echo '</div>';

            echo '<div class="form-group">';
                echo form_label('Пароль', "pswd1");
                echo form_password($pswd1) . '<br/>';
                echo form_password($pswd2);
            echo '</div>';

            echo '<div class="form-group">';
                echo form_label('Имя', 'fname');
                echo form_input($fname, set_value('fname'));
            echo '</div>';

            echo '<div class="form-group">';
                echo form_label('Фамилия','lname');
                echo form_input($lname,set_value('lname'));
            echo '</div>';

            echo form_submit(array('id' => 'submit', 'name' => 'submit', 'class' => 'btn btn-primary'),"Регистрация");
        form_close();
?>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
        <a class="thumbnail" id="vk_link" href="<?php echo $vk_uri; ?>" data-toggle="popover" data-placement="right" data-content="Регистрация через ВКонтакте"
           data-trigger="hover" data-original-title="Вконтакте"><img src="/images/vk.JPG" alt="vk" width="32" height="32"></a>

        <a class="thumbnail" id="fb_link" href="#" data-toggle="popover" data-placement="right" data-content="Регистарция через Facebook"
           data-trigger="hover" data-original-title="Facebook"><form method="post" action="<?php echo site_url('account/create_fb'); ?>"><div id="fb_reg_form"></div></form><img src="/images/fb.png" alt="fb" width="32" height="32"></a>
    </div>
</div>
