<div class="text-center text-danger">
    <h1 class="glyphicon glyphicon-warning-sign">Ошибка</h1>
    <div class="text-danger">У вас нет доступа к этой странице!</div>
    <?php
    if(isset($error_message)){
        echo '<div>Сообщение: <strong>' . $error_message . '</strong></div>';
    }
    ?>
</div>