<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="/js/print_users.js"></script>
<link rel="stylesheet" href="/css/print_users.css">


<div class="col-lg-6 col-lg-offset-3">
    <h4>Предварительно сформированные команды (по рейтингу)</h4>

    <div>
        <ul id="sortable1" class="connectedSortable">
            <?php
            foreach($teams as $team) {
                echo '<li class="ui-state-default" data-id="' .
                    $team[0]['id'] . '" data-rating="'.
                    $team[0]['value'] . '">' . $team[0]['fname'] . ' ' .
                    $team[0]['lname'] . '</li>';
            }
            ?>
        </ul>

        <ul id="sortable2" class="connectedSortable">
            <?php
            foreach($teams as $team) {
                echo '<li class="ui-state-default" data-id="' .
                    $team[1]['id'] . '" data-rating="'.
                    $team[1]['value'] . '">' . $team[1]['fname'] . ' ' .
                    $team[1]['lname'] . '</li>';
            }
            ?>
        </ul>
    </div>

    <form id="teams_form" method="post" action="<?php echo site_url('admin/tourney/set_teams'); ?>">
        <input type="hidden" name="teams" id="teams_list">
        <input type="hidden" name="tourney" value="<?php echo $tourney_id; ?>">
        <button class="btn btn-success" id="submitBtn">Далее</button>
    </form>
</div>
