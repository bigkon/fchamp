<script src="/js/create_tourney.js"></script>
<link rel="stylesheet" href="/css/create_tourney.css">

<h1 class="text-center">Создание турнира</h1><br/>
<div class="col-lg-6 col-lg-offset-3">
    <div id="errors_display" class="alert alert-danger">
        <a type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</a>
        <?php echo validation_errors(); ?>
        <?php if( isset($error) ) echo $error; ?>
    </div>
    <?php echo form_open('', array('class' => 'col-lg-6', 'name'=>'form_tourney', 'method'=>'post', 'action'=>site_url('admin/create_tourney'))); ?>
        <h3 class="text-muted text-center">Турнир</h3><br/>
    <div class="form-group">
        <?php echo form_label('Название','tourney_name'); ?>
        <?php echo form_input(array('name'=>'tourney_name', 'class'=>'form-control'),set_value('tourney_name')); ?>
    </div>
    <div class="form-group">
        <?php echo form_label('Описание','tourney_desc'); ?>
        <?php echo form_textarea(array('name'=>'tourney_desc', 'class'=>'form-control', 'rows'=>'5'),set_value('tourney_desc'));?>
    </div>

    <div class="form-group">
        <?php echo form_label('Тип', 'tourney_state'); ?>
        <?php echo form_dropdown('tourney_state', $state_select, '2', 'class="form-control"'); ?>
    </div>
    <?php echo form_hidden('players');?>
    <div class="form-group">
        <?php echo form_submit(array('name'=>'submit_btn', 'class'=>'btn btn-primary', 'id'=>'submit_btn'),'Далее'); ?>
    </div>

    <?php echo form_close(); ?>

    <div class="col-lg-6">
        <h3 class="text-muted text-center">Участники</h3>
       <div id="players_list">

           <?php
            foreach($players as $player) {?>
                <div class="player_select" player_id="<?php echo $player->id; ?>"><?php echo $player->lname . ' ' . $player->fname; ?><i class="pull-right"></i></div>
            <?php } ?>

        </div><br/>
        <div><i class="btn btn-default btn-xs pull-right" onclick="select_all_click();">Выделить всех</i></div>
    </div>
</div>
