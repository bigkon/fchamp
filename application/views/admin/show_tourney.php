<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="/js/tourney_page.js"></script>
<link rel="stylesheet" href="/css/admin_css.css">

<h2 class="text-center">Турнир</h2>

<hr/>
<div class="row">
    <div class="col-lg-4 col-lg-offset-1">
        <div class="col-lg-6">
            <label>Имя: </label>
            <input class="form-control" value="<?php echo $tourney['name']; ?>" id="tourney-name">
        </div>
        <div class="col-lg-6">
            <label>Состояние: </label>
            <select class="form-control" id="tourney-state">
                <?php
                foreach(array_keys($tourney_states) as $key) :
                    echo '<option ' . ($key==$tourney['state'] ? 'selected ' : '') . 'value="' . $key . '">' . $tourney_states[$key] . '</option>';
                endforeach;
                ?>
            </select>
        </div>
    </div>
    <div class="col-lg-4">
        <label>Описание: </label>
        <textarea class="form-control" id="tourney-desc"><?php echo $tourney['description']; ?></textarea>
    </div>
    <div class="col-lg-2 pull-right">
        <button class="btn btn-default" title="Сохранить изменения" id="update-tourney">
            <span class="glyphicon glyphicon-edit"></span>
        </button>
    </div>
</div>
<hr/>

<div class="col-lg-5">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">Раунды
                <span id="example" class="glyphicon glyphicon-question-sign pull-right" title="Drag-n-Drop для изменеие порядка раундов в турнире."></span>
                <button id="rounds_order_change" style="display: none;"><span class="glyphicon glyphicon-ok"></span></button>
            </div>
            <table class="table items-list" data-table="round" id="rounds-table">
                <thead>
                    <tr>
                        <th>Раунд</th><th>Тип</th><th>Игр</th><th>Состояние</th>
                    </tr>
                </thead>
                <tbody id="sortable">

                <?php
                $rounds_url = site_url('admin/rounds/show');
                foreach($rounds as $round):?>
                    <tr data-id="<?php echo $round['id_round']; ?>">
                        <td><span class="glyphicon glyphicon-remove delete-round">&nbsp;</span><?php echo $round['queue']; ?></td>
                        <td><a href="<?php echo $rounds_url . '/' . $round['id_round']; ?>"><?php echo $round_types[$round['playoff_type']]; ?></a></td>
                        <td><?php echo $round['games']; ?></td>
                        <td><?php echo $round_states[$round['state']];?></td>
                    </tr>
                <?php endforeach; unset($rounds_url); ?>

                </tbody>
            </table>
            <div class="panel-footer">
                <div class="col-lg-6">Добавить раунд:</div>
                <div class="col-lg-1 col-lg-offset-4">
                    <button id="create_round" class="btn btn-default">
                        <span class="glyphicon glyphicon-plus"></span>
                    </button>
                </div>
                <div class="row">
                    <div class="col-lg-2">
                        <label>№:</label>
                        <input id="round_queue" type="text" class="form-control">
                    </div>
                    <div class="col-lg-5">
                        <label>Тип:</label>
                        <select id="round_playoff_type" class="form-control">
                            <option value="0">Регулярный</option>
                            <option value="1">Play-off</option>
                        </select>
                    </div>
                    <div class="col-lg-5">
                        <label>Состояние:</label>
                        <select id="round_state" class="form-control">
                            <option value="0">Не начался</option>
                            <option value="1">Идет</option>
                            <option value="2">Закончился</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">Команды</div>

            <table class="table items-list" data-table="team" id="teams-table">
                <thead>
                    <tr>
                        <th>Название</th>
                    </tr>
                </thead>
                <tbody>

                <?php
                $teams_url = site_url('admin/teams/show');
                foreach($teams as $team):?>
                    <tr data-id="<?php echo $team['id']; ?>">
                        <td>
                            <span class="glyphicon glyphicon-remove delete-team">&nbsp;</span>
                            <a href="<?php echo $teams_url . '/' . $team['id'] ?>"><small><?php echo $team['name']; ?></small></a></td>
                    </tr>
                <?php endforeach; ?>

                </tbody>
            </table>
            <div class="panel-footer">
                <div>Добавить команду:</div>
                <div class="input-group">
                    <select name="add_team" class="form-control input-sm">
                    <option value="">---------</option>
                    <?php foreach($teams_not_in_tourney as $team):
                        echo '<option value="' . $team['id'] . '">' . $team['name'] . '</option>';
                    endforeach;
                    ?>
                    </select>
                    <span class="input-group-btn">
                        <button id="add_team" class="btn btn-default">
                            <span class="glyphicon glyphicon-plus"></span>
                        </button>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">Участники</div>
            <table class="table items-list" data-table="account" id="users-table">
                <thead>
                    <tr>
                        <th>Имя</th>
                    </tr>
                </thead>
                <tbody>

                <?php
                $users_url = site_url('admin/users/show');
                foreach($users as $user): ?>
                    <tr data-id="<?php echo $user['id']; ?>">
                        <td><a href="<?php echo $users_url . '/' . $user['id']; ?>"><?php echo $user['lname'] . ' ' . $user['fname']; ?></a></td>
                    </tr>
                <?php endforeach; unset($users_url); ?>

                </tbody>
            </table>
            <div class="panel-footer"></div>
        </div>
    </div>
</div>


<div class="col-lg-7">
    <div>
        <div class="panel panel-default">
            <div class="panel-heading">Турнирная таблица</div>

            <table class="table items-list" data-table="team" id="tourney_table">
                <thead>
                    <tr>
                        <th>Команда</th>
                        <th>Игры</th>
                        <th>Победы</th>
                        <th>Паражения</th>
                        <th>Очки</th>
                        <th>Голы</th>
                        <th>Разница</th>
                    </tr>
                </thead>
                <tbody>

                <?php
                foreach($tourney_table as $row): ?>
                    <tr data-id="<?php echo $row['id']; ?>">
                        <td><a href="<?php echo $teams_url . '/' . $row['id']; ?>"><small><?php echo $row['team']; ?></small></a></td>
                        <td><?php echo $row['games']; ?></td>
                        <td><?php echo $row['wins']; ?></td>
                        <td><?php echo $row['loses']; ?></td>
                        <td><?php echo $row['points']; ?></td>
                        <td><?php echo $row['goals_from'] . ' - ' . $row['goals_to']; ?></td>
                        <td><?php echo $row['diff']; ?></td>
                    </tr>
                <?php endforeach; unset($teams_url);?>

                </tbody>
            </table>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">Play-off</div>
        <div class="panel-body"></div>
        <div class="panel-footer"></div>
    </div>
</div>