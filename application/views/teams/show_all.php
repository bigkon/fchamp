<style>
    .table tr td:first-child {
        border-right: 1px solid lightgray;
    }
</style>

<script>
    $(function(){
        $('#search-btn').click(function(){

            var u_name = $('#user-name').val();
            var t_state = $('#team-state').val();
            alert(JSON.stringify({user:u_name, state:t_state}));
        });
    });
</script>

<h1 class="text-center">Команды</h1>
<div class="col-lg-8 col-lg-offset-2">
    <div class="row thumbnail">
        <div class="pull-right">
            <input id="search-btn" class="btn btn-default" type="button" value="Искать">
        </div>
        <div class="col-lg-1"><strong>Фильтры</strong></div>
        <div class="col-lg-4 col-lg-offset-1">
            <input id="user-name" class="form-control" type="text" placeholder="Имя участника">
        </div>
        <div class="col-lg-4">
            <select class="form-control" id="team-state">
                <option value="">---------</option>
                <option value="1">Активная</option>
                <option value="0">Не активная</option>
            </select>
        </div>
    </div>
    <table class="table table-hover">
        <thead><tr><th>Команда</th><th colspan="2">Участники</th></tr></thead>
        <tbody>

        <?php
        $show_team_url = site_url('teams/show') . "/";
        $show_user_url = site_url('account/show') . '/';
        foreach($teams as $team): ?>
            <tr>
                <td>
                    <a href="<?php echo $show_team_url . $team['team_id']; ?>"><?php echo $team['team_name']; ?></a>
                </td>
                <td>
                    <a href="<?php echo $show_user_url . $team['user_a_id']; ?>">
                        <img height="32" src="/images/users/<?php echo $team['user_a_photo']; ?>">
                        <?php echo $team['user_a_lname'] . ' ' . $team['user_a_fname']; ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo $show_user_url . $team['user_b_id']; ?>">
                        <img height="32" src="/images/users/<?php echo $team['user_b_photo']; ?>">
                        <?php echo $team['user_b_lname'] . ' ' . $team['user_b_fname']; ?>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>

        </tbody>
    </table>
</div>