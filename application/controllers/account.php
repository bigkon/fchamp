<?php
Class Account extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->data['active_page'] = 'users_link';
    }

    public function create()
    {
        $this->data['title'] = 'Регистрация';

        if( $this->session->userdata('logged') )
        {
            $this->data['error_message'] = 'вы уже зарегистрировались';
            $this->render_page('access_denied');
            return FALSE;
        }

        $this->load->library('form_validation');
        $this->load->helper('form');
        $rules = array(
            array(
            'field' => 'email',
            'label' => 'Email',
            'rules' =>'required|valid_email|is_unique[users.email]',
            ),
            array(
                'field' => 'pswd1',
                'label' => 'Пароль',
                'rules' => 'required|matches[pswd2]'
            ),
            array(
                'field' => 'pswd2',
                'label' => 'Подтверждение пароля',
                'rules' =>'required|regex_match[/^[0-9A-Za-z!@#$%*]{6,16}$/]'
            ),

            array(
                'field' => 'fname',
                'label' => 'Имя',
                'rules' =>'required|regex_match[/^[А-Я][а-я]+$/ui]'
            ),
            array(
                'field' => 'lname',
                'label' => 'Фамилия',
                'rules' =>'required|regex_match[/^[А-Я][а-я]+$/ui]'
            )
        );
        $this->load->database();
        $this->form_validation->set_rules($rules);

        if( !$this->form_validation->run() ) {
            $input_params = array();
            foreach($rules as $rule){
                $input_params[] = $rule['field'];
                $input_params[$rule['field']] = array(
                    'id' => $rule['field'],
                    'name' => $rule['field'],
                    'placeholder' => $rule['label'],
                    'class' => 'form-control');
            }
            $input_params['email']['type'] = 'email';
            $input_params['vk_uri'] = "https://oauth.vk.com/authorize?client_id=4376528&scope=4194304&" .
                "redirect_uri=" . site_url('account/create_vk') . "&response_type=code&v=5.21";

            $this->data = array_merge($this->data, $input_params);
            $this->render_page('account/show_register');
        } else {
            $this->load->model('account_model');
            $user_id = $this->account_model->register_user(array(
                'email' => $this->input->post('email'),
                'pswd' => hash('sha256', $this->input->post('pswd1') . SALT),
                'fname' => mb_convert_case($this->input->post('fname'), MB_CASE_TITLE),
                'lname' => mb_convert_case($this->input->post('lname'), MB_CASE_TITLE)/*,
                'is_active' => 1*/
            ));

            $this->load->library('encrypt');
            $enc_data = urlencode($this->encrypt->encode($user_id . '|' . $this->input->post('email')));
            $this->data['message'] = 'Для активации своего аккаунта на <a href="' . site_url('account/create') . '">F-Champ</a>' .
            ' перейдите по ссылке: <br/><a href="' . site_url('account/activate') . '/' . $enc_data . '">' . $enc_data . '</a>';
            $this->data['email'] = $this->input->post('email');
            $this->render_page('account/register_email');
        }
    }

    public function create_vk()
    {
        $this->data['title'] = 'Регистрация через ВК';

        if( isset($_GET['code']) ) {
            $uri2 = "https://oauth.vk.com/access_token?code={$_GET['code']}&client_id=4376528&" .
                "client_secret=mvNNiY10xZxUtEsv3XsE&redirect_uri=" . site_url('account/create_vk');

            $params = json_decode(file_get_contents($uri2), true);

            $uid = $params['user_id'];
            $email = $params['email'];

            if(isset($uid)){
                $uri3 = "https://api.vk.com/method/users.get?uid=$uid&fields=photo_200";
                $params = json_decode(file_get_contents($uri3), true);
                $params = $params['response'][0];
                $data = array(
                    'uid' => $uid,
                    'email' => $email,
                    'fname' => $params['first_name'],
                    'lname' => $params['last_name'],
                    'photo' => $params['photo_200']
                );

                $this->load->model('account_model');

                if( ! $this->account_model->checkByEmail($data['email']) ) {
                    $this->data['error_message'] = 'пользователь с такими данными уже существует';
                    $this->render_page('access_denied');
                } else {
                    $data['logged'] = TRUE;

                    $user_data = array(
                        'email' => $data['email'],
                        'fname' => $data['fname'],
                        'lname' => $data['lname'],
                        'photo' => $data['email'] . '.jpg' ,
                        'is_active' => 1,
                        'auth_network' => 1,
                        'auth_id' => $data['uid']
                    );

                    copy($data['photo'], 'images/users/' . $user_data['photo']);

                    $data['id'] = $this->account_model->register_user($user_data);
                    $this->session->set_userdata($data);
                    redirect(site_url('account/show'));
                }

            } else {
                echo "UID не установлен!";
            }
        } else {
            $this->render_page('access_denied');
        }

    }

    public function create_fb()
    {
        if( $this->input->post('id') == null ){
            $this->render_page('access_denied');
            return false;
        }

        $uid = $this->input->post('id');
        $email = $this->input->post('email');
        $fname = $this->input->post('first_name');
        $lname = $this->input->post('last_name');
        $photo = $this->input->post('picture');

        $this->load->model('account_model');
        if( $this->account_model->checkByEmail($email) )
        {
            $id = $this->account_model->register_user(array(
                'email' => $email,
                'fname' => $fname,
                'lname' => $lname,
                'photo' => $email . '.jpg',
                'auth_network' => 2,
                'auth_id' => $uid,
                'is_active' => 1
            ));

            copy($photo, 'images/users/' . $email . '.jpg');

            $this->session->set_userdata('logged', TRUE);
            $this->session->set_userdata('id', $id);
            $this->session->set_userdata('fname', $fname);
            $this->session->set_userdata('lname', $lname);

            echo json_encode(array('success' => true));
        } else {
            echo json_encode(array("success" => FALSE, "message" => "Такой аккаунт уже есть!" ));
        }
    }

    public function show($id = null)
    {
        $this->data['title'] = 'Личный кабинет';

        if( $this->session->userdata('id') == null ) {
            $this->render_page('access_denied');
            return FALSE;
        }


        $this->load->model('account_model');
        if($id == null){
            $id = $this->session->userdata('id');
        }

        if($user_info = $this->account_model->get($id)){
            $user_info = (array)$user_info;
            $user_info['self'] = $id == $this->session->userdata('id');

            $this->data = array_merge($this->data, $user_info);
            $this->data['teams'] = $this->account_model->get_teams($id);
            $this->data['tourney_list'] = $this->account_model->get_tourneys($id);

            if($user_info['self']) {
                //получить список голосований
                $this->data['available_votes'] = $this->account_model->get_available_votes($id);
            }
            $this->render_page('account/show');

        } else {
            $this->data['error_message'] = 'такого пользователя не существует';
            $this->render_page('access_denied');
        }
    }

    public function show_all()
    {
        $this->load->model('account_model');
        $this->data['users'] = $this->account_model->get();

        $this->render_page('account/show_all');
    }

    public function login()
    {
        $this->load->model('account_model');

        $email = $this->input->post('emailLogin');
        $pswd = $this->input->post('passwordLogin');

        if( ($user_data = $this->account_model->login($email, hash('sha256', $pswd . SALT))) ) {
            if( !$user_data->is_active ) {
                //$this->render_page('access_denied');
                $this->output->set_output(json_encode(array('result'=>0, 'error'=>"Этот аккаунт не активирован.")));
                return;
            }
            $this->session->set_userdata('logged', TRUE);
            $this->session->set_userdata($user_data);
            //redirect(site_url($this->input->post('cur_page')));
            $this->output->set_output(json_encode(array('result'=>1)));
        } else {
            $this->output->set_output(json_encode(array('result'=>0, 'error'=>"Неверные данные")));
        }
    }

    public function login_net()
    {
        $uid = $this->input->get('uid');
        $auth_net = $this->input->get('auth_net');
        $this->load->model('account_model');
        if( $user_data = $this->account_model->loginNet($uid, $auth_net) ) {
            $this->session->set_userdata('logged', TRUE);
            $this->session->set_userdata($user_data);
            $this->output->set_output(json_encode( array(
                    'result' => 1
            )));
        } else {
            $this->output->set_output(json_encode( array(
                    'result' => 0, 'error' => 'Такого пользователя не сущетсвует.'
                )
            ));
        }

    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('/account/create');
    }

    public function edit()
    {
        $this->data['title'] = 'Редактировать';
        if( $this->session->userdata('id') == null ){
            $this->render_page('access_denied');
            return FALSE;
        }
        $this->load->model('account_model');

        $this->load->helper('form');
        $this->load->library('form_validation');

        $rules = array(
            array(
                'field' => 'fname',
                'label' => 'Имя',
                'rules' =>'required|regex_match[/^[А-Я][а-я]+$/ui]'
            ),
            array(
                'field' => 'lname',
                'label' => 'Фамилия',
                'rules' =>'required|regex_match[/^[А-Я][а-я]+$/ui]'
            ),
            array(
                'field' => 'pswd',
                'label' => 'Пароль',
                'rules' => 'matches[pswd_check]'
            ),
            array(
                'field' => 'pswd',
                'label' => 'Пароль',
                'rules' => 'regex_match[/^[0-9A-Za-z!@#$%*]{6,16}$/]'
            )
        );
        $this->form_validation->set_rules($rules);

        if( $this->form_validation->run() ) {
            $this->load->model('account_model');
            $update_data = array(
                'fname' => mb_convert_case($this->input->post('fname'), MB_CASE_TITLE),
                'lname' => mb_convert_case($this->input->post('lname'), MB_CASE_TITLE)
            );

            //проверяем пароль
            if( $this->input->post('pswd') != null ) {
                if( !$this->account_model->check_pswd($this->session->userdata('id'),hash('sha256', $this->input->post('pswd_old') . SALT)) ) {
                    $this->data['error_pswd'] = 'Пароль введен не верно';
                } else {
                    $update_data['pswd'] = hash('sha256', $this->input->post('pswd') . SALT);
                }
            }

            //проверяем фото
            if( $_FILES['photo']['size'] > 0 ) {
                $allowed_exts=['gif', 'jpeg', 'jpg', 'bmp', 'png'];
                $temp = explode(".", $_FILES["photo"]["name"]);
                $extension = end($temp);
                if( in_array($extension, $allowed_exts) && $_FILES['photo']['size'] < 102400 ) {
                    if( $_FILES['photo']['error'] == 0 )
                    {
                        $email = $this->account_model->getEmail($this->session->userdata('id'));
                        move_uploaded_file($_FILES['photo']['tmp_name'],
                            'images/users/' . $email . '.' . $extension);
                        $update_data['photo'] = $email . '.' . $extension;
                    }
                } else {
                    $this->data['error_photo'] = 'Некорректный файл';
                }
            }

            $this->account_model->edit($this->session->userdata('id'), $update_data);
            $this->session->set_userdata(['fname' => $update_data['fname'], 'lname' => $update_data['lname']]);

            $user_data = $this->account_model->get($this->session->userdata('id'));
            $this->data = array_merge($this->data, (array)$user_data);
            $this->render_page('account/edit');
        } else {
            $user_data = $this->account_model->get($this->session->userdata('id'));
            $this->data = array_merge($this->data, (array)$user_data);
            $this->render_page('account/edit');
        }
    }

    public function activate($encrypted)
    {
        $encrypted = urldecode($encrypted);
        $this->load->library('encrypt');
        $user_data = explode('|', $this->encrypt->decode($encrypted), 2);
        $this->load->model('account_model');
        if($this->account_model->activate($user_data[0], $user_data[1]) == 1) {
            echo 'Аккаунт успешно активирован';
        } else {
            echo 'Что то пошло не так.';
        }
    }

    public function vote($tourney_id)
    {
        $user_id = $this->session->userdata('id');
        if( !$user_id ) {
            $this->render_page('access_denied');
            return;
        }
        $this->load->model('account_model');

        $access = false;
        foreach($this->account_model->get_available_votes($user_id) as $tmp) {
            if($tmp['id'] == $tourney_id) {
                $access = true;
                break;
            }
        }
        if( !$access ) {
            $this->data['error_message'] = 'Голосование по этому турниру запрещено.';
            $this->render_page('access_denied');
            return;
        }

        if( ($votes = json_decode($this->input->post('votes'))) != null) {
            $users_cnt = count($this->account_model->users_by_tourney($tourney_id)) - 1;
            $vote_data = array();
            for($i=0; $i<$users_cnt; $i++) {
                $vote_data[] = array(
                    'id_user_to'=>$votes[$i],
                    'value'=>round(100*($users_cnt-$i)/$users_cnt)
                );
            }
            $res = $this->account_model->vote($user_id, $vote_data, $tourney_id);

            $response = array('success'=>$res);
            if( !$res ) {
                $response['error_message'] = 'Не удалось записать результаты голосования.';
            }
            $this->output->set_output(json_encode($response));
            return;
        }

        $this->data['votes'] = $this->account_model->list_vote($user_id, $tourney_id);
        $this->render_page('account/vote');
    }

}