<?php
class Admin_ajax extends CI_Controller
{
    protected function is_admin()
    {
        return $this->session->userdata('is_admin');
    }

    /*-----------------tourney-----------------*/

    public function update_tourney()
    {
        if(!$this->is_admin()) {
            $response = array('success'=>false, 'error_message'=>'Нет доступа');
            $this->output->set_output(json_encode($response));
            return;
        }
        $tourney_id = $this->input->post('tourney_id');
        $name = $this->input->post('tourney_name');
        $state = $this->input->post('tourney_state');
        $desc = $this->input->post('tourney_desc');

        $this->load->model('tourney_model');
        $res = $this->tourney_model->update($tourney_id, $name, $state, $desc);
        $response = array('success' => $res);
        if(!$res) {
            $response['error_message'] = "Не удалось изменить данные турнира.";
        }
        $this->output->set_output(json_encode($response));
    }

    /*-----------------rounds-----------------*/

    public function create_round()
    {
        if(!$this->is_admin()) {
            $response = array('success'=>false, 'error_message'=>'Нет доступа');
            $this->output->set_output(json_encode($response));
            return;
        }
        $round = json_decode($this->input->post('round'));

        $id_tourney = $round->tourney_id;
        $queue = $round->round_queue;
        $playoff_type = $round->round_playoff_type;
        $state = $round->round_state;

        $this->load->model('rounds_model');
        $round_id = $this->rounds_model->create($id_tourney, $queue, $playoff_type, $state);
        $result = array('success' => 1, 'round_id' => $round_id);
        $this->output->set_output(json_encode($result));
    }

    public function delete_round()
    {
        if(!$this->is_admin()) {
            $response = array('success'=>false, 'error_message'=>'Нет доступа');
            $this->output->set_output(json_encode($response));
            return;
        }
        $round_id = $this->input->post('round_id');
        $this->load->model('rounds_model');
        $res = $this->rounds_model->delete($round_id);
        $response = array('success' => $res);
        if(!$res) {
            $response['error_message'] = "Не удалось добавить команду в турнир.";
        }
        $this->output->set_output(json_encode($response));
    }

    public function update_round()
    {
        if(!$this->is_admin()) {
            $response = array('success'=>false, 'error_message'=>'Нет доступа');
            $this->output->set_output(json_encode($response));
            return;
        }
        $round_id = $this->input->post('round_id');
        $queue = $this->input->post('round_queue');
        $playoff_type = $this->input->post('round_playoff_type');
        $state = $this->input->post('round_state');

        $this->load->model('rounds_model');
        $res = $this->rounds_model->update($round_id, $queue, $playoff_type, $state);
        $response = array('success' => $res);
        if(!$res) {
            $response['error_message'] = "Не удалось изменить настройки раунда.";
        }
        $this->output->set_output(json_encode($response));
    }

    public function update_round_order()
    {
        if(!$this->is_admin()) {
            $response = array('success'=>false, 'error_message'=>'Нет доступа');
            $this->output->set_output(json_encode($response));
            return;
        }
        $rounds = json_decode($this->input->post('rounds'));
        $this->load->model('rounds_model');
        $res = $this->rounds_model->update_order($rounds);
        if($res == null) $res=1;
        $response = array('success' => $res);
        if(!$res) {
            $response['error_message'] = "Не удалось изменить порядок раундов.";
        }
        $this->output->set_output(json_encode($response));
    }

    /*-----------------teams-----------------*/

    public function add_team()
    {
        if(!$this->is_admin()) {
            $response = array('success'=>false, 'error_message'=>'Нет доступа');
            $this->output->set_output(json_encode($response));
            return;
        }
        $team_id = $this->input->post('team_id');
        $tourney_id = $this->input->post('tourney_id');
        $this->load->model('tourney_model');
        $res = $this->tourney_model->add_team($tourney_id, $team_id);
        $response = array('success' => $res);
        if(!$res) {
            $response['error_message'] = "Не удалось добавить команду в турнир.";
        } else {
            $this->load->model('account_model');
            $response['users'] = $this->account_model->users_by_team($team_id);
            $this->account_model->add_to_vote($response['users'], $tourney_id);
        }
        $this->output->set_output(json_encode($response));
    }

    public function delete_team()
    {
        if(!$this->is_admin()) {
            $response = array('success'=>false, 'error_message'=>'Нет доступа');
            $this->output->set_output(json_encode($response));
            return;
        }
        $team_id = $this->input->post('team_id');
        $tourney_id = $this->input->post('tourney_id');
        $this->load->model('teams_model');
        $response['users'] = $this->teams_model->get_users($team_id);
        $this->load->model('account_model');
        $this->account_model->delete_from_vote($response['users'], $tourney_id);
        $this->load->model('tourney_model');
        $res = $this->tourney_model->delete_team($tourney_id, $team_id);
        $response['success'] = $res;
        if(!$res) {
            $response['error_message'] = 'Не удалось удалить команду из турнира.';
        }
        $this->output->set_output(json_encode($response));
    }
}