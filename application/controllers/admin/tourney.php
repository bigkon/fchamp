<?php

class Tourney extends MY_Controller
{
    protected function _shuffle_players($id_list)
    {
        $this->load->model('account_model');
        $users = $this->account_model->get_users_rating(implode(',', $id_list));

        $cnt = count($users);
        $rand = round( $cnt * (rand(2,4) / 10) );
        $value = isset($users[$rand]['value']) ? $users[$rand]['value'] -1 : 1;

        $no_rating_ids = array();
        $id_cnt = count($id_list);
        for($i=0; $i<$id_cnt; $i++) {
            $contains = false;
            for($j=0; $j<$cnt; $j++) {
                if($id_list[$i] == $users[$j]['id']) {
                    $contains = true;
                    break;
                }
            }
            if( !$contains ) {
                $no_rating_ids[] = $id_list[$i];
            }
        }

        if( !empty($no_rating_ids) )
            array_splice($users, $rand, 0, $this->account_model->get_users_name( implode(',', $no_rating_ids), $value));

        unset($no_rating_ids, $rand, $cnt, $value, $id_cnt);

        for($i=0; $i<count($users); $i++) {
            $rating_groups[ $users[$i]['value']/10 ] [] = $users[$i];
        }

        $teams = array();
        while( !empty($rating_groups) ) {
            $teams[] = $this->_rand_team($rating_groups);
        }

        $this->data['teams'] = $teams;
        $this->render_page('admin/print_users');
    }

    protected function _rand_team(&$users)
    {
        $max_rating_key = max( array_keys($users) );
        $min_rating_key = min( array_keys($users) );

        if($max_rating_key != $min_rating_key) {
            $max_user_key = array_rand($users[$max_rating_key]);
            $min_user_key = array_rand($users[$min_rating_key]);
        } else {
            $tmp = array_rand($users[$max_rating_key], 2);
            $max_user_key = $tmp[0];
            $min_user_key = $tmp[1];
        }

        $max_user = $users[$max_rating_key][$max_user_key];
        $min_user = $users[$min_rating_key][$min_user_key];

        unset($users[$max_rating_key][$max_user_key], $users[$min_rating_key][$min_user_key]);

        if( empty($users[$max_rating_key]) )
            unset($users[$max_rating_key]);


        if( empty($users[$min_rating_key]) )
            unset($users[$min_rating_key]);

        return array($max_user, $min_user);
    }



    public function __construct()
    {
        parent::__construct();
        $this->data['active_page'] = 'admin_link';
        $this->data['title'] = 'Турниры';
    }

    public function create()
    {
        $this->data['title'] = 'Новый чемпионат';

        if( !$this->session->userdata('is_admin') ) {
            $this->data['error_message'] = 'Доступ к этой странице есть только у администратора.';
            $this->render_page('access_denied');
            return;
        }

        $this->data['state_select'] = array('0'=>'Не начался', '1'=>'Текущий', '3'=>'Закончился');
        $this->load->model('account_model');
        $this->data['players'] = $this->account_model->get();
        $this->load->helper('form');
        $this->load->library('form_validation');

        $rules = array(
            array('field' => 'tourney_name',
                'label' => 'Название турнира',
                'rules' => 'required|max_length[30]'),

            array('field' => 'tourney_desc',
                'label' => 'Описание',
                'rules' => 'max_length[200]'
            ),

            array('field' => 'tourney_state',
                'label' => 'Тип',
                'rules' => 'required|greater_than[-1]|less_than[4]'
            )
        );
        $this->form_validation->set_rules($rules);
        if( !$this->form_validation->run() )
        {
            $this->render_page('admin/create_tourney');
        } else {
            $players_id = json_decode($this->input->post('players'));
            $cnt = count( $players_id->player_id );
            if($cnt == 0 || $cnt%2 != 0) {
                $this->data['error'] = '<p>Нужно выбрать четное колличество участников!</p>';
                $this->render_page('admin/create_tourney');
                return;
            }

            $this->load->model('tourney_model');
            $tourney_id = $this->tourney_model->create(
                $this->input->post('tourney_name'),
                $this->input->post('tourney_desc'),
                $this->input->post('tourney_state'));
            $this->data['tourney_id'] = $tourney_id;

            $this->_shuffle_players( $players_id->player_id );
        }
    }

    public function set_teams()
    {
        $teams = json_decode($this->input->post('teams'));
        $tourney_id = $this->input->post('tourney');
        $cnt = count($teams);
        $this->load->model('tourney_model');
        for($i=0; $i<$cnt; $i++) {
            $this->tourney_model->team_to_tourney($teams[$i]->id_user_a, $teams[$i]->id_user_b, $tourney_id);
        }

        redirect( site_url("admin/tourney/show/$tourney_id") );
    }

    public function show($tourney_id)
    {
        $this->load->model('tourney_model');

        $this->data['tourney'] = $this->tourney_model->get($tourney_id);

        $this->data['rounds'] = $this->tourney_model->rounds_by_tourney($tourney_id);

        $this->data['teams'] = $this->tourney_model->teams_by_tourney($tourney_id);
        $this->data['teams_not_in_tourney'] = $this->tourney_model->teams_not_in_tourney($tourney_id);

        $this->data['users'] = $this->tourney_model->users_by_tourney($tourney_id);

        $this->data['tourney_table'] = $this->tourney_model->tourney_table($tourney_id);

        $this->data['round_states'] = array('0'=>'Не начался', '1'=>'Идет', '2'=>'Закончился');
        $this->data['round_types'] = array('0'=>'Регулярный', '1'=>'Play-off');
        $this->data['tourney_states'] = array('0'=>'Не начался', '1'=>'Идет', '2'=>'Голосование', '3'=>'Окончен');

        $this->render_page('/admin/show_tourney');
    }


}