<?php
class Teams extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->data['active_page'] = 'teams_link';
        $this->data['title'] = 'Команды';
    }

    public function show_all()
    {
        $this->load->model('teams_model');
        $result = $this->teams_model->get_all();
        $this->data['teams'] = $result;
        $this->render_page('teams/show_all');
    }
}

