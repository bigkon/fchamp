<?php
class MY_Controller extends CI_Controller
{
    public $data = array('title' => 'Default');
    public function __construct()
    {
        parent::__construct();
    }

    public function render_page($view)
    {
        $logged = $this->session->userdata('logged');
        $is_admin = $this->session->userdata('is_admin');

        if ( $logged && $is_admin) {
            $this->load->view('templates/header_admin', $this->data);
        } elseif ( $logged ) {
            $this->load->view('templates/header_logged', $this->data);
        } else {
            $this->load->view('templates/header_default', $this->data);
        }

        $this->load->view($view, $this->data);

        $this->load->view('templates/footer');
    }

}