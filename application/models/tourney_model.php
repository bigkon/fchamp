<?php
class Tourney_model extends CI_Model
{
    CONST TABLE_NAME = "tourneys";

    public function get($id)
    {
        return $this->db->get_where($this::TABLE_NAME, array('id'=>$id))->row_array();
    }

    public function tourney_table($tourney_id)
    {
        $query = "CALL tourney_table($tourney_id);";
        $query = $this->db->query($query);
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function create($name, $desc, $state)
    {
        $this->db->insert($this::TABLE_NAME, array(
            'name' => $name,
            'description' => $desc,
            'state' =>$state));
        return $this->db->insert_id();
    }

    /*
     * Добавляет команду из 2 игроков к турниру, если команды нету, создает
     */
    public function team_to_tourney($id_user_a, $id_user_b, $tourney_id)
    {
        $query = $this->db->query("CALL set_team($id_user_a, $id_user_b, $tourney_id);");
        $query->next_result();
        $query->free_result();
    }

    public function teams_not_in_tourney($tourney_id)
    {
        $query = "CALL teams_not_in_tourney($tourney_id);";
        $query = $this->db->query($query);
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function teams_by_tourney($tourney_id)
    {
        $query = "CALL teams_by_tourney($tourney_id);";
        $query = $this->db->query($query);
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function users_not_in_tourney($tourney_id)
    {
        $query = "CALL users_not_in_tourney($tourney_id);";
        $query = $this->db->query($query);
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function users_by_tourney($tourney_id)
    {
        $query = "CALL users_by_tourney($tourney_id);";
        $query = $this->db->query($query);
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function rounds_by_tourney($tourney_id)
    {
        $query = "CALL rounds_by_tourney($tourney_id);";
        $query = $this->db->query($query);
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    /*------------------------------------------------*/

    public function add_team($tourney_id, $team_id)
    {
        return $this->db->insert('tourney_teams', array(
            'id_tourney' => $tourney_id,
            'id_team' => $team_id
        ));
    }

    public function delete_team($tourney_id, $team_id)
    {
        $this->db->where('id_tourney', $tourney_id);
        $this->db->where('id_team', $team_id);
        return $this->db->delete('tourney_teams');
    }

    public function update($tourney_id, $name, $state, $desc)
    {
        $this->db->where('id', $tourney_id);
        return $this->db->update($this::TABLE_NAME, array(
            'name' => $name,
            'description' => $desc,
            'state' => $state
        ));
    }

}