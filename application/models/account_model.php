<?php
class Account_model extends CI_Model {

    const TABLE_NAME = 'users';

    public function checkByEmail($email)
    {
        $query = $this->db->get_where($this::TABLE_NAME, array('email' => $email), 1);
        if ( $query->num_rows > 0 ) {
            return FALSE;
        }
        return TRUE;
    }

    public function register_user($user_data)
    {
        $this->db->insert('users', $user_data);
        return $this->db->insert_id();
    }

    public function login($email, $pswd)
    {
        $this->db->select('id, fname, lname, is_admin, is_active');
        $result = $this->db->get_where($this::TABLE_NAME, array(
            'email' => $email,
            'pswd' => $pswd
        ), 1);

        if($result->num_rows == 0){
            return false;
        } else {
            $result = $result->result()[0];
            return $result;
        }

    }

    public function get($id=null)
    {
        $select_fields = 'id, fname, lname, photo';
        $this->db->select($select_fields);
        if ( $id == null ) {
            return $this->db->get($this::TABLE_NAME)->result();
        }
        $this->db->where('id', $id)->from($this::TABLE_NAME);

        $result = $this->db->get()->result();
        return count($result) > 0 ? $result[0] : false;
    }

    public function getEmail($id)
    {
        $this->db->select('email')->
            where('id', $id)->
            from($this::TABLE_NAME);

        $result = $this->db->get()->result();
        return count($result) > 0 ? $result[0]->email : false;
    }

    public function edit($id, $data)
    {
        $this->db->where('id',$id)->
        update($this::TABLE_NAME, $data);
    }

    public function loginNet($uid, $auth_net)
    {
        $result = $this->db->get_where($this::TABLE_NAME, array(
            'auth_network' => $auth_net,
            'auth_id' => $uid
        ), 1);

        if($result->num_rows == 0) {
            return false;
        } else {
            $result = $result->result()[0];
            return $result;
        }
    }

    public function check_pswd($id, $pswd)
    {
        $result = $this->db->get_where($this::TABLE_NAME, array('id' => $id, 'pswd' => $pswd), 1);
        return $result->num_rows;
    }

    public function activate($id, $email)
    {
        $this->db->where('id', $id);
        $this->db->where('email', $email);
        $this->db->update($this::TABLE_NAME, array('is_active'=>1));
        return $this->db->affected_rows();
    }

    public function get_users_name($id_list, $rating)
    {
        $query = 'SELECT id, fname, lname, ' . $rating . ' as "value" FROM users WHERE id IN(' . $id_list . ');';
        return $this->db->query($query)->result_array();
    }

    public function get_users_rating($users_id = null)
    {
        if($users_id == null)
            $query = 'CALL get_users_rating();';
        else
            $query = 'CALL get_rating_by_ids("' . $users_id . '");';
        $query = $this->db->query($query);
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function users_by_team($team_id)
    {
        $query = $this->db->query("CALL users_by_team( $team_id );");
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function get_available_votes($id_user)
    {
        $query = $this->db->query("CALL get_available_votes( $id_user );");
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function get_teams($id_user)
    {
        $query = $this->db->query("CALL get_user_teams( $id_user ); ");
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function get_tourneys($id_user)
    {
        $query = $this->db->query("CALL tourneys_by_user( $id_user ); ");
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function list_vote($id_user, $id_tourney)
    {
        $query = $this->db->query("CALL list_vote( $id_user, $id_tourney ); ");
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function users_by_tourney($id_tourney)
    {
        $query = $this->db->query("CALL users_by_tourney( $id_tourney ); ");
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function vote($id_user_from, $votes, $tourney_id)
    {
        foreach($votes as $vote) {
            $this->db->query(
                "update votes set " .
                "value=(value*count+{$vote['value']})/(count+1), s" .
                "count=count+1 " .
                "where id_tourney=$tourney_id " .
                "and id_user_to={$vote['id_user_to']}");
        }
        return $this->db->insert('votes_done', array('id_user'=>$id_user_from, 'id_tourney'=>$tourney_id));
    }

    public function add_to_vote($users, $tourney_id)
    {
        $this->db->insert('votes', array('id_user_to'=>$users[0]['id'], 'id_tourney'=>$tourney_id));
        $this->db->insert('votes', array('id_user_to'=>$users[1]['id'], 'id_tourney'=>$tourney_id));
    }

    public function delete_from_vote($users, $tourney_id)
    {
        $this->db->where('id_tourney', $tourney_id);
        $this->db->where("(id_user_to={$users['id_user_a']} OR id_user_to={$users['id_user_b']})");
        $this->db->delete('votes');
    }

}