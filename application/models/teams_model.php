<?php
class Teams_Model extends CI_Model
{
    CONST TABLE_NAME = 'teams';

    public function get_all()
    {
        $query = $this->db->query("CALL get_teams");
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function get_users($team_id)
    {
        $this->db->select('id_user_a, id_user_b');
        $this->db->where('id', $team_id);
        return $this->db->get($this::TABLE_NAME)->row_array();
    }
}