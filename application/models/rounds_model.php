<?php
class Rounds_model extends CI_Model
{
    const TABLE_NAME = "rounds";

    public function create($tourney_id, $queue, $playoff_type, $state)
    {
        //изменить всем следующим очередь +1
        $this->db->query('update rounds set queue=queue+1 where id_tourney=' . $tourney_id . ' and queue>=' . $queue);

        $this->db->insert($this::TABLE_NAME, array(
            'id_tourney' => $tourney_id,
            'queue' => $queue,
            'playoff_type' => $playoff_type,
            'state' => $state,
        ));
        return $this->db->insert_id();
    }

    public function delete($round_id)
    {
        $this->db->where('id', $round_id);
        return $this->db->update($this::TABLE_NAME, array('id_tourney'=>'0'));
    }

    public function update($round_id, $queue, $playoff_type, $state)
    {
        $this->db->where('id', $round_id);
        return $this->db->update($this::TABLE_NAME, array(
            'queue' => $queue,
            'playoff_type' => $playoff_type,
            'state' => $state
        ));
    }

    public function update_order($rounds)
    {
        return $this->db->update_batch($this::TABLE_NAME, $rounds, 'id');
    }

}