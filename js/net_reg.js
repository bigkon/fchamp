<!--START Facebook initialization scripts-->

window.fbAsyncInit = function() {
    FB.init({
        appId      : '748863955144578',
        xfbml      : false,
        version    : 'v2.0'
    });
    FB.getLoginStatus();
};

(function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
js = d.createElement(s); js.id = id;
js.src = "//connect.facebook.net/ru_RU/sdk.js";
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

<!--END Facebook initialization scripts-->


$('#vk_link,#fb_link,#pswd1').popover();

$(function(){
    $errors = $('#errors_display');
    if($errors.children().length <= 1)
    $errors.hide();

    $('#fb_link').click(function(evt){
        evt.preventDefault();

        FB.login(function(response) {
            if(response.status == 'connected'){
                FB.api('/me?fields=id,email,first_name,last_name', function(response) {
                    var data = response;
                    FB.api('me/picture?width=200&height=200',function(response){
                        data.picture=response.data.url;
                        $.post(window.location.protocol+'//'+window.location.host+'/account/create_fb', data, callback_fb_reg, 'json');
                    });
                });
            } else {
                alert(response.status);
            }
        }, {scope: 'public_profile,email'});
    });
});

function callback_fb_reg(result){
    if( result.success == true ) {
        window.location = window.location.protocol+'//'+window.location.host+'/account/show';
    } else {
        $('#errors_display').show().append('<p>' + result.message + '</p>');
    }
}
