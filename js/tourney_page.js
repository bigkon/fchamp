var url = window.location.protocol + '//' + window.location.host + '/admin/admin_ajax/';
var tourney_id = window.location.href.substr(window.location.href.lastIndexOf('/') + 1);
$(function(){
    $( "#sortable" ).sortable({
        placeholder: "ui-state-highlight",
        helper: function(e, ui) {
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui;
        },
        update: function() {
            $('#rounds_order_change').fadeIn();
        }
    }).disableSelection();


    $('#update-tourney').click(function() {
        var name = $('#tourney-name').val();
        var state = $('#tourney-state').val();
        var desc = $('#tourney-desc').val();
        $.post(
            url + 'update_tourney',
            { tourney_id: tourney_id, tourney_name: name, tourney_state: state, tourney_desc:desc },
            function(resp) {
                if(!resp.success) {
                    alert(resp.error_message);
                }
            },
            'json'
        );
    });


    $(document).on( 'click', '.delete-round', function(e){
        var round_id = $(this).parent().parent().data('id');
        var self = this;
        $.post(
            url + 'delete_round',
            'tourney_id=' + tourney_id + '&round_id=' + round_id,
            function(resp) {
                if(resp.success) {
                    $(self).parent().parent().remove();
                } else {
                    alert(resp.error_message);
                }
            },
            'json'
        );
    } );


    $(document).on( 'click', '.delete-team', function(e) {
        var row = $(this).parent().parent();
        var team_id = row.data('id');
        var self = this;
        $.post(
            url + 'delete_team',
            { tourney_id: tourney_id, team_id: team_id },
            function(resp){
                if(resp.success) {
                    row.remove();
                    $('#users-table').find('[data-id="' + resp.users.id_user_a + '"]').remove();
                    $('#users-table').find('[data-id="' + resp.users.id_user_b + '"]').remove();
                    $('select[name=add_team]').append('<option value="' + team_id + '">' + $(self).next().text() + '</option>');
                } else {
                    alert(resp.error_message);
                }
            },
            'json'
        );
    });


    $('#add_team').click(function() {
        var select = $('select[name=add_team]');
        var team_id = select.find('option:selected').val();
        if(team_id) {
            $.post(
                url + 'add_team',
                { tourney_id: tourney_id, team_id: team_id },
                function(resp) {

                    if(resp.success) {
                        var teams_table = $('#teams-table');
                        teams_table.append('<tr data-id="' + team_id + '">' +
                            '<td>' +
                            '<span class="glyphicon glyphicon-remove delete-team">&nbsp;</span>' +
                            '<a href="' + window.location.protocol+'//'+window.location.host+'/admin/teams/show/'+team_id +
                            '"><small>' + $(select).find('option:selected').text() + '</small></a></td>' +
                            '</tr>');
                        $(select).find('option:selected').remove();

                        var users_table = $('#users-table');
                        users_table.append('<tr data-id="' + resp.users[0].id + '"><td><a href="' +
                            window.location.protocol+'//'+window.location.host+'/admin/users/show/'+resp.users[0].id
                            + '">' + resp.users[0].lname + ' ' + resp.users[0].fname + '</a></td></tr>');
                        users_table.append('<tr data-id="' + resp.users[1].id + '"><td><a href="' +
                            window.location.protocol+'//'+window.location.host+'/admin/teams/show/'+resp.users[1].id
                            + '">' + resp.users[1].lname + ' ' + resp.users[1].fname + '</a></td></tr>');
                    } else {
                        alert(resp.error_message);
                    }
                },
                'json'
            )
        }
    });


    $('#example').tooltip();


    $('#rounds_order_change').click(function(){ //обновление порядка раундов в турнире
        var q_order = Array(), q=1;
        $('#rounds-table tbody tr').each(function(){
            q_order.push({id: $(this).data('id'), queue: q++});
        });
        self = this;
        $.post(url + 'update_round_order',
            'rounds=' + JSON.stringify(q_order),
            function(resp) {
                if(resp.success) {
                    $(self).hide();
                    $('<small class="label label-success">Изменения сохранены</small>').appendTo($(self).parent()).fadeOut(2000, function(){
                        $(this).remove();
                    });
                } else {
                    $(self).hide();
                    $('<small class="label label-danger">' + resp.error_message + '</small>').appendTo($(self).parent()).fadeOut(2000, function(){
                        $(self).remove();
                    });
                }
            },
            'json'
        );
    });


    $('#create_round').click(function(){
        var round_data = ({
            tourney_id: tourney_id,
            round_queue: $('#round_queue').val(),
            round_state: $('#round_state').val(),
            round_playoff_type: $('#round_playoff_type').val()
        });

        $.post(
            url + 'create_round',
            'round=' + JSON.stringify(round_data),
            function(resp) {
                if(!resp.success) {
                    alert(resp.error_message);
                    return;
                }
                var round_id = resp.round_id;
                $('#rounds-table').append('<tr data-id="'+round_id+'">' +
                    '<td><span class="glyphicon glyphicon-remove delete-round">&nbsp;</span>'+round_data.round_queue+'</td>' +
                    '<td><a href="' + window.location.protocol+'//'+window.location.host+'/admin/rounds/show/'+round_id + '">'+$("#round_playoff_type option:selected").text()+'</a></td>' +
                    '<td>0</td>' +
                    '<td>'+$("#round_state option:selected").text()+'</td></tr>');
            },
            'json'
        );
    });

});