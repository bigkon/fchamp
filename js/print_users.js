$(function() {
    $( "#sortable1, #sortable2" ).sortable({
        dropOnEmpty: true,
        connectWith: ".connectedSortable"
    }).disableSelection();
});

$(function(){
    $('#submitBtn').click(function(e){
        e.preventDefault();
        var teams = Array();
        $ul1 = $('#sortable1>li');
        $ul2 = $('#sortable2>li');
        var cnt = $ul1.length;
        if( cnt != $ul2.length) {
            alert('Команды сформированы не правильно. Повторите попытку.');
            return;
        }

        for(i=0; i<$ul1.length; i++) {
            teams[i] = {'id_user_a' : $($ul1[i]).data('id'), 'id_user_b' : $($ul2[i]).data('id')};
        }

        $form = $('#teams_form');
        $('#teams_list').val(JSON.stringify(teams));
        $form.submit();
    });
});