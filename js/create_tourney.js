function select_all_click() {
    $('.player_select').click();
}

$(function() {

    $errors = $('#errors_display');
    if($errors.children().length <= 1)
        $errors.hide();

    $('.player_select').click(function() {
        $(this).children().toggleClass('glyphicon glyphicon-ok');
        $(this).toggleClass('item_selected');
    });

    $('#submit_btn').click(function(e) {
        e.preventDefault();
        var players_input = $('input[name=players]');
        var players_selected = $('.item_selected');
        var values = Array();

        for(var i=0; i<players_selected.length; i++) {
            values.push(players_selected[i].getAttribute('player_id'));
        }
        if(values.length < 1 || values.length % 2 != 0)
        {
            $errors.append('<p>Нужно выбрать четное колличество участников!</p>').show();
            return;
        }
        players_input.val(JSON.stringify({'player_id' : values}));

        $('form[name=form_tourney]').submit();
    });
});