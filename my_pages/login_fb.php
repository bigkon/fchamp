<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '748863955144578',
            xfbml      : false,
            version    : 'v2.0'
        });

        /*FB.getLoginStatus(function(response) {
            if(response.status == 'connected'){
                alert("Вы добавили приложение");
            } else if(response.status == 'not_authorized') {
                alert("Нужно добавить приложение");
            }
        });*/

        FB.getLoginStatus();
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/ru_RU/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

</script>

<script>
    function login_btn_click(){
        FB.login(function(response) {
            if(response.status == 'connected'){
                //alert('Вы успешно зашли!');
                FB.api('/me?fields=id,email,first_name,last_name', function(response) {

                    var data = response;
                    FB.api('me/picture?width=200&height=200',function(response){
                        data.picture=response.data.url;

                        //alert(JSON.stringify(data));
                        var print_res = function(result){
                            alert(result);
                        }

                        $.post('ajax_fb.php',data,print_res,'text');
                    });
                });
            } else {
                alert(response.status);
            }

        }, {scope: 'public_profile,email'});
    }

    function logout_btn_click(){
        FB.api(
            {
                method: 'auth.revokeExtendedPermission',
                perm: 'email'//perms.join(',')
            },
            function(response) {
                console.log(response);
            }
        );

        FB.api('/me/permissions', 'delete', function(response) {
            console.log(response); // true
        });
    }
</script>

<button type="button" onclick="login_btn_click();">Войти</button>

<button type="button" onclick="logout_btn_click();">Выйти</button>

<div id="test"></div>
